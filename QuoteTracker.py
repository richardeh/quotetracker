"""
QuoteTracker.py
Pulls and Tracks Equity Quotes

Author: Richard Harrington
Date Created: 9/9/2013
Last Updated: 9/14/2013
"""

# TODO: make the list auto-update

from http.client import *
from re import search
from tkinter import *

class Stock():
    last_sale=0
    netchange=0
    percent_change=0
    change_direction="white"
    
    def connect(self,symbol):
        # Opens a data connection and returns the decoded html document
        url='www.nasdaq.com'
        req='/symbol/'+symbol+'/real-time'
        conn=HTTPConnection(host=url)
        try:
            conn.request('GET',req)
            content=conn.getresponse().read()
            content=content.decode(encoding='utf-8')
            return content
        except:
            print ('Connection failed')
        
    def get_quote(self,content):
        # Reads the content and matches on the quote data
        match=re.search('id="qwidget_lastsale".*?>(.*?)<',content)
        if match:
            self.last_sale=match.group(1)
        else:
            return False

    def net_change(self,content):
        # Reads the content and matches on the net change data
        match=re.search('id="qwidget_netchange.*?class="qwidget-cents qwidget-(.*?)".*?>(.*?)<',content)
        
        if match:
            self.netchange=match.group(2)
            if "Green" in match.group(1):
                self.change_direction="green"
            elif match.group(1)=="Red":
                self.change_direction="red"
            else:
                pass
        
    def pct_change(self,content):
        # Reads the content and matches on the percent-change data
        match=re.search('id="qwidget_percent".*?>(.*?)<',content)
        if match:
            self.percent_change=match.group(1)
        else:
            pass

    def add_symbol(self,symbol):
        # Adds symbols to the list of symbols
        symbols.append(symbol)
        self.update(symbol)
    
    def update(self,symbol):
        # Ties together the connection and the processing functions for
        # a single function call
        data=self.connect(symbol)
        self.get_quote(data)
        self.net_change(data)
        self.pct_change(data)

    def validate_symbol(self,symbol):
        # validates a symbol before tracking it
        data=self.connect(symbol)
        if self.get_quote(data)==False:
            return False
        else:
            self.add_symbol(symbol)
            return True

root=Tk()
symbols=["ctl","msft","goog","aapl"]   
e=Entry(root)
e.pack()
e.focus_set()


def callback():
    # Adds a stock to the list.
    symbol=e.get()
    if s.validate_symbol(symbol):
        make_label(symbol)
    
def make_label(symbol):
    # Builds the label for each symbol
    labelstring=symbol.upper()+": "+s.last_sale+" "+s.netchange+" "+s.percent_change
    label=Label(group,text=labelstring,anchor=W,width=25,bg=s.change_direction)
    label.pack()

def update_labels():
    pass

b=Button(root,text="Add Symbol",command=callback)
b.pack()
    
s=Stock()
group=LabelFrame(root,text="Listings",padx=5,pady=5)
for symbol in symbols:
    s.update(symbol)
    make_label(symbol)

group.pack(padx=10,pady=10,anchor=W)

root.title("Quote Tracker")
root.mainloop()

